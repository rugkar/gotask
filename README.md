# Go tech task for Junior Go developer position 

## Description and goal

### Task problem

Given file data.txt with any number of lines and any number of words separated by space ( ' ' ) symbol in each line. Words contain only alphanumeric ( a-zA-Z0-9 ) symbols. The program should produce another file res.txt which has the same lines in the same order but each line now has its' words in order.

### Goal

Improve the given code by:

    1. Fixing bugs
    2. Improving readability
    3  Improving reusability
    4. Improving performance

## Functions

## readFile(fileName string)

### Description
This function reads each byte of the file, once it finds a '\n' symbol, it appends the line of the file to the content array and goes on to read a new line.
### Parameters
**fileName** - the name of the file that is going to be read.
### Returns
A string slice with all of the lines of the file.

## writeResult(fileName string, output []string)

### Description
This function writes the result of the program to a sellected file.
### Parameters
**fileName** - the name of the file you want to write to

**output** - the array of strings that you want to write to the file
### Returns
Nothing

## sortLine(line string, index int, output []string, wg *sync.Waitroup)

### Description
This function sorts one line of words that are separated with ' ' alphabetically and case insensitive. After the line is sorted it updates its' value in the 'output' slice.
### Parameters
**line** - the line of words that is going to be sorted

**index** - the index where the line is in the 'output' slice

**output** - array of all the lines

**wg** - waitgroup

### Returns
nothing


## Unit tests

There is a test file 'main_test.go'. 

Only the **sortLine(line string, index int, output []string, wg *sync.WaitGroup)** is tested and it has 6 test cases. 
