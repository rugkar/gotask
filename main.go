package main

import (
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
	"sync"
)

func main() {
	//reading data from file
	data := readFile("data.txt")

	//creating a new waitgroup and adding the number of lines we need to sort
	var wg sync.WaitGroup
	wg.Add(len(data))

	//sort all lines
	for v, k := range data {
		go sortLine(k, v, data, &wg)
	}
	//wait until all lines are sorted
	wg.Wait()

	//writing result to file
	writeResult("res.txt", data)
}

func sortLine(line string, index int, lines []string, wg *sync.WaitGroup) {
	words := strings.Fields(line)
	sort.Slice(words, func(i, j int) bool {
		return strings.ToLower(words[i]) < strings.ToLower(words[j])
	})
	lines[index] = strings.Join(words, " ") + "\n"
	wg.Done()
}

func readFile(fileName string) []string {
	var lines []string
	var line string
	content, err := ioutil.ReadFile(fileName)

	if err != nil {
		log.Fatal(err)
	}

	for _, k := range content {
		if k != '\n' {
			line += string(k)
		} else {
			lines = append(lines, line)
			line = ""
		}
	}
	return lines
}

func writeResult(fileName string, output []string) {
	file, err := os.Create(fileName)

	if err != nil {
		log.Fatal(err)
	}

	for v := range output {
		for _, c := range []byte(output[v]) {
			file.Write([]byte{c})
		}
	}
}
