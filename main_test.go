package main

import (
	"sync"
	"testing"
)

func TestSorting(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{
			input:    "gamma alpha beta",
			expected: "alpha beta gamma\n",
		},
		{
			input:    "Woof meow tWeet squeek moO coAk t00t quack BLUB owowow Ringdingdingdingdingeringeding",
			expected: "BLUB coAk meow moO owowow quack Ringdingdingdingdingeringeding squeek t00t tWeet Woof\n",
		},
		{
			input:    "bbbb aaaa ffff dddd aabb oooo AAAAA",
			expected: "aaaa AAAAA aabb bbbb dddd ffff oooo\n",
		},
		{
			input:    "5 6 8 9 4 2 1 0 2 4 2",
			expected: "0 1 2 2 2 4 4 5 6 8 9\n",
		},
		{
			input:    "ZZ JJ A S a b Z z k",
			expected: "A a b JJ k S Z z ZZ\n",
		},
		{
			input:    "zz85 as54 as65 as42 b45 as47 as45",
			expected: "as42 as45 as47 as54 as65 b45 zz85\n",
		},
	}

	output := make([]string, 6)

	var wg sync.WaitGroup
	wg.Add(len(tests))

	for k, v := range tests {
		t.Run(v.input, func(t *testing.T) {
			sortLine(v.input, k, output, &wg)
			if output[k] != v.expected {
				t.Error("Test failed: \n inputed: ", v.input, "\n expected: ", v.expected, "received: ", output[k])
			}
		})
	}
}
